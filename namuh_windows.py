# -*- coding: utf-8 -*-
import os
import sys

from namuh_structure import *


def resource_path(relative_path):  # exe 리소스 경로 찾기
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


os.environ['PATH'] = ';'.join([os.environ['PATH'], resource_path('bin')])


class WinDllWmca:
    def __init__(self):
        self.wmca_dll = WinDLL(r'bin\wmca.dll', use_last_error=True)
        self.load()

    def connect(self, hwnd, sz_id, sz_pw, sz_cert_pw):  # 접속 후 로그인(인증)
        func = self.wmca_dll.wmcaConnect
        func.argtypes = [HWND, DWORD, CHAR, CHAR, LPSTR, LPSTR, LPSTR]
        func.restype = BOOL
        result = func(hwnd, CA_WMCAEVENT, b"T", b"W", sz_id, sz_pw, sz_cert_pw)
        # print("connect =", bool(result))
        return bool(result)

    def disconnect(self):  # 접속 해제
        func = self.wmca_dll.wmcaDisconnect
        func.argtypes = []
        func.restype = BOOL
        result = func()
        # print("disconnect =", bool(result))
        return bool(result)

    def is_connected(self):  # 접속 여부 확인
        func = self.wmca_dll.wmcaIsConnected
        func.argtypes = []
        func.restype = BOOL
        result = func()
        # print("is_connected =", bool(result))
        return bool(result)

    def query(self, hwnd, tr_id, sz_tr_code, sz_input, n_input_len, n_account_index=0):  # 서비스(TR) 호출
        func = self.wmca_dll.wmcaQuery
        # HWND hWnd, int nTRID, constchar* szTRCode, const char* szInput, int nInputLen, int nAccountIndex
        func.argtypes = [HWND, INT, LPSTR, LPSTR, INT, INT]
        func.restype = BOOL
        result = func(hwnd, int(tr_id), sz_tr_code.encode(), sz_input.encode(), int(n_input_len), int(n_account_index))
        # print("query =", bool(result))
        return bool(result)

    def attach(self, hwnd, sz_bc_type, sz_input, n_code_len, n_input_len):  # 실시간 등록
        func = self.wmca_dll.wmcaAttach
        # HWND hWnd, const char* szBCType, const char* szInput, int nCodeLen, int nInputLen
        func.argtypes = [HWND, LPSTR, LPSTR, INT, INT]
        func.restype = BOOL
        result = func(hwnd, sz_bc_type.encode(), sz_input.encode(), n_code_len, n_input_len)
        # print("attach =", bool(result))
        return bool(result)

    def detach(self, hwnd, sz_bc_type, sz_input, n_code_len, n_input_len):  # 실시간 취소
        func = self.wmca_dll.wmcaDetach
        # HWND hWnd, const char* szBCType, const char* szInput, int nCodeLen, int nInputLen
        func.argtypes = [HWND, LPSTR, LPSTR, INT, INT]
        func.restype = BOOL
        result = func(hwnd, sz_bc_type.encode(), sz_input.encode(), n_code_len, n_input_len)
        # print("detach =", bool(result))
        return bool(result)

    def detach_window(self, hwnd):  # 실시간 일괄 취소
        func = self.wmca_dll.wmcaDetachWindow
        func.argtypes = [HWND]
        func.restype = BOOL
        result = func(hwnd)
        # print("detach_window =", bool(result))
        return bool(result)

    def detach_all(self):  # 실시간 일괄 취소
        func = self.wmca_dll.wmcaDetachAll
        func.argtypes = []
        func.restype = BOOL
        result = func()
        # print("detach_all =", bool(result))
        return bool(result)

    def load(self):  # dll 실행
        func = self.wmca_dll.wmcaLoad
        func.argtypes = []
        func.restype = BOOL
        result = func()
        # print("load =", bool(result))
        return bool(result)

    def free(self):  # dll 종료
        func = self.wmca_dll.wmcaFree
        func.argtypes = []
        func.restype = BOOL
        result = func()
        # print("free =", bool(result))
        return bool(result)

    def set_order_pwd(self, pass_out, pass_in):  # dll 거래 비밀번호 셋팅
        # func = self.wmca_dll.wmcaSetOrderPwd
        func = self.wmca_dll.wmcaSetOrderPwd
        func.argtypes = [LPSTR, LPSTR]
        func.restype = BOOL
        # result = func(pass_out, pass_in)
        result = func(pass_out, pass_in.encode())

        return bool(result)

    def set_account_index_pwd(self, pass_out, account_index, pass_in):  # dll 계좌 비밀번호 셋팅
        # func = self.wmca_dll.wmcaSetAccountIndexPwd
        # func.argtypes = [c_char_p, INT, c_char_p]
        func = self.wmca_dll.wmcaSetAccountIndexPwd
        func.argtypes = [LPSTR, INT, LPSTR]
        func.restype = BOOL
        # result = func(pass_out, account_index, pass_in)
        result = func(pass_out, account_index, pass_in.encode())

        return bool(result)


if __name__ == '__main__':
    pass
