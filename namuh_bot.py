# -*- coding: utf-8 -*-
import traceback
import typing
from ctypes import wintypes

import keyring
from PyQt5 import uic, QtCore, QtGui, sip
from PyQt5.QtCore import pyqtSlot, QThread, QObject
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox, QWidget, QDesktopWidget, QAbstractItemView, QTableWidgetItem

from namuh_windows import *


def set_sys_log_messages(msg):  # try except 에러 메시지
    time = QtCore.QTime.currentTime()
    time_text = time.toString('[HH : mm : ss] ') + msg
    print(time_text)
    main.dashboard.ptxt_sys_log.appendPlainText(time_text)


def print_except_messages():  # try except 에러 메시지
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    print(traceback.format_exc())


class NamuhNativeEventFiler:
    def wnd_proc_callback(self, event_type, message):
        msg = wintypes.MSG.from_address(message.__int__())
        if event_type == "windows_generic_MSG":
            if msg.message == CA_WMCAEVENT:  # DLL MFC 콜백 처리
                data = {}
                if msg.wParam == CA_CONNECTED:  # 로그인 성공
                    print('로그인 성공')
                    data = {CA_CONNECTED, True}
                elif msg.wParam == CA_SOCKETERROR:  # 통신 오류 발생
                    set_sys_log_messages('통신 오류 발생')
                    data = {CA_SOCKETERROR, False}
                elif msg.wParam == CA_RECEIVEDATA:  # 서비스 응답 수신(TR)
                    set_sys_log_messages('서비스 응답 수신(TR)')
                    tr_index, block_name, block_data = self.on_wm_receivedata(msg.lParam)
                    # print(sz_block_name, sz_block_data)
                    data = {CA_RECEIVEDATA: {'tr_index': tr_index, 'block_name': block_name, 'block_data': block_data}}
                elif msg.wParam == CA_RECEIVESISE:  # 실시간 데이터 수신(BC)
                    # print("실시간 데이터 수신(BC)")
                    tr_index, now_stock_name, now_stock_data = self.on_wm_receivesise(msg.lParam)
                    data = {CA_RECEIVESISE: {'tr_index': tr_index, 'now_stock_name': now_stock_name, 'now_stock_data': now_stock_data}}
                elif msg.wParam == CA_RECEIVEMESSAGE:  # 상태 메시지 수신 (입력값이 잘못되었을 경우 문자열형태로 설명이 수신됨)
                    tr_index, msg_cd, user_msg = self.on_wm_receivemessage(msg.lParam)
                    data = {CA_RECEIVEMESSAGE: {'tr_index': tr_index, 'msg_cd': msg_cd, 'user_msg': user_msg}}
                elif msg.wParam == CA_RECEIVECOMPLETE:  # 서비스 처리 완료
                    set_sys_log_messages('서비스 처리 완료')
                    data = {CA_RECEIVECOMPLETE, True}
                elif msg.wParam == CA_RECEIVEERROR:  # 서비스 처리중 오류 발생 (입력값 오류등)
                    set_sys_log_messages('서비스 처리중 오류 발생 (입력값 오류등)')
                    data = {CA_RECEIVEERROR, True}
                return data

    def on_wm_receivemessage(self, lparam):
        tr_index, msg_cd, user_msg = None, None, None

        try:
            msg = OutdatablockStruct.from_address(lparam)
            data = ReceivedStruct.from_buffer(msg.pData.contents)
            string_buffer = create_string_buffer(data.szData[:85], 85)
            msg_header = MsgHeaderStruct.from_buffer(string_buffer)
            tr_index = msg.TrIndex
            msg_cd = msg_header.msg_cd.decode("cp949")
            user_msg = msg_header.user_msg.decode("cp949").strip()
            # print("상태 메시지 수신 (입력값이 잘못되었을 경우 문자열형태로 설명이 수신됨) = {1} : {2}".format(msg.TrIndex, msg_cd, user_msg))
        except Exception as e:
            print_except_messages()

        return tr_index, msg_cd, user_msg

    @staticmethod
    def on_wm_receivedata(lparam):  # DLL 메시지 수신(1회성)
        tr_index, query_name, query_data = None, None, None

        try:
            msg = OutdatablockStruct.from_address(lparam)
            recevie_data = ReceivedStruct.from_buffer(msg.pData.contents)
            data_len = recevie_data.nLen
            tr_index = msg.TrIndex
            string_buffer = create_string_buffer(recevie_data.szData[:data_len], data_len)
            query_name = recevie_data.szBlockName.decode("cp949")

            struct_type = None
            if query_name == 'c1101OutBlock':
                struct_type = (C1101OutBlockStruct * 1)
            elif query_name == 'c1101OutBlock2':
                struct_type = (C1101OutBlock2Struct * (data_len // sizeof(C1101OutBlock2Struct)))
            elif query_name == 'c1101OutBlock3':
                struct_type = (C1101OutBlock3Struct * 1)
            elif query_name == 'c4113OutKospi200':
                struct_type = (C4113OutKospi200Struct * 1)
            elif query_name == 'p1005OutBlock':
                struct_type = (P1005OutBlockStruct * (data_len // sizeof(P1005OutBlockStruct)))
            elif query_name == 'c8102InBlock':
                struct_type = (C8102InBlockStruct * 1)
            elif query_name == 'c8102OutBlock':
                struct_type = (C8102OutBlockStruct * 1)
            elif query_name == 'c8201OutBlock':
                struct_type = (C8201OutBlockStruct * 1)
            elif query_name == 'c8201OutBlock1':
                struct_type = (C8201OutBlockStruct1 * (data_len // sizeof(C8201OutBlockStruct1)))

            query_data = [data.get_dict() for data in struct_type.from_buffer(string_buffer)]
            # print(f"{p_msg.TrIndex}, {sz_block_name}, {n_len}, {repr(sz_data[:])}")
            # print(repr(sz_data))
            # if sz_block_name == "c1101OutBlock":
            #     print("'" + sz_data.get_str("code") + "'")
            #     print("'" + sz_data.get_str("hname") + "'")

        except Exception as e:
            print_except_messages()

        return tr_index, query_name, query_data

    @staticmethod
    def on_wm_receivesise(lparam):  # DLL 메시지 수신(실시간)
        tr_index, attach_name, attach_data = None, None, None

        try:
            msg = OutdatablockStruct.from_address(lparam)
            data = ReceivedStruct.from_buffer(msg.pData.contents)
            tr_index = msg.TrIndex
            string_buffer = create_string_buffer(data.szData, data.nLen)
            attach_name = data.szBlockName[:2]
            if attach_name == b"j8":  # 코스피 주식 현재가(실시간) 수신
                attach_data = J8OutBlockStruct.from_buffer(string_buffer, 3)
            elif attach_name == b"d2":  # 실시간 체결통보 => 실시간 체결통보는 별도로 Attach()함수를 호출하지 않아도 자동 수신
                attach_data = D2OutBlockStruct.from_buffer(string_buffer, 3)
            elif attach_name == b"d3":  # 실시간 체결통보 => 실시간 체결통보는 별도로 Attach()함수를 호출하지 않아도 자동 수신
                attach_data = D3OutBlockStruct.from_buffer(string_buffer, 3)

            # print(f"{now_stock_name} : {repr(now_stock_data)}")
        except Exception as e:
            print_except_messages()

        return tr_index, attach_name, attach_data.get_dict()


class DashboardThread(QThread):  # DLL 서버 연결시 까지 대기 후 체결 정보 실시간 등록 처리
    is_run = True

    def __init__(self, parent: typing.Optional[QObject] = ...) -> None:
        super().__init__(parent)
        self.parent = parent

    def run(self) -> None:
        while self.is_run:
            if wmca.is_connected():
                self.parent.set_init_buy_list()
                self.is_run = False
            self.msleep(1)


class DashboardWidget(QWidget):
    TR_TBL_BUY_LIST = 1  # 체결 종목 리스트 TR INDEX

    """"""
    now_timer = None
    th_is_run = True
    """"""
    BUY_LIST = []

    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent

        uic.loadUi(resource_path('form.ui') + '/dashboard.ui', self)
        self.show()

        # tablewidget 세팅
        column_headers = ['종목명', '종목코드', '시간', '등락부호', '등락폭', '현재가', '등락률', '고가', '저가', '매도호가', '매수호가', '거래량', '거래량전일비', '변동거래량', '거래대금', '시가', '가중평균가', '장구분']
        self.tbl_buy_list.setColumnCount(len(column_headers))
        # self.tbl_buy_list.setRowCount(10)
        self.tbl_buy_list.setHorizontalHeaderLabels(column_headers)
        self.tbl_buy_list.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # self.setTableWidgetData()

    def show(self) -> None:
        super().show()
        self.now_timer = QtCore.QTimer()
        self.now_timer.setInterval(1000)
        self.now_timer.timeout.connect(self.show_time)
        self.now_timer.start()

        self.th = DashboardThread(self)
        self.th.start()

    @pyqtSlot()
    def show_time(self):
        # 현재시간 동기화
        time = QtCore.QTime.currentTime()
        second = time.second() % 2 == 0
        text = time.toString("HH mm ss" if second else "HH:mm:ss")
        self.num_now_time.display(text)

    def set_tbl_buy_list_row_data(self, data):
        items = self.tbl_buy_list.findItems(data.get('code'), QtCore.Qt.MatchExactly)
        if not items:
            row_pos = self.tbl_buy_list.rowCount()
            self.tbl_buy_list.insertRow(row_pos)
        else:
            row_pos = items[0].row()

        col = 1
        for key, val in data.items():
            cell = QTableWidgetItem(str(val))
            self.tbl_buy_list.setItem(row_pos, col, cell)
            col += 1

    def set_init_buy_list(self):
        hwnd = int(self.winId())
        c8201_in = C8201InBlockStruct()
        c8201_in.bnc_bse_cdz1 = b'1'
        pass_out = create_string_buffer(44)
        wmca.set_order_pwd(pass_out, '')
        c8201_in.pswd_noz8 = pass_out.value

        wmca.query(hwnd=hwnd, tr_id=self.TR_TBL_BUY_LIST, sz_tr_code='c8201', sz_input=c8201_in.get_bytes().decode(), n_input_len=sizeof(c8201_in), n_account_index=1)

    def nativeEvent(self, event_type: typing.Union[QtCore.QByteArray, bytes, bytearray], message: sip.voidptr) -> typing.Tuple[bool, int]:
        msg_data = NamuhNativeEventFiler().wnd_proc_callback(event_type, message)
        if msg_data:
            if CA_RECEIVEMESSAGE in msg_data:
                set_sys_log_messages(msg_data.get(CA_RECEIVEMESSAGE).get('user_msg'))
            elif CA_RECEIVEDATA in msg_data:  # 체결 종목 실시간 데이터 요청
                if msg_data.get(CA_RECEIVEDATA).get('tr_index') == self.TR_TBL_BUY_LIST:
                    block_data = msg_data.get(CA_RECEIVEDATA).get('block_data')
                    block_name = msg_data.get(CA_RECEIVEDATA).get('block_name')
                    if block_name == 'c8201OutBlock1':
                        for data in block_data:
                            if data.get('issue_codez6'):
                                wmca.attach(hwnd=int(self.winId()), sz_bc_type='j8', sz_input=data.get('issue_codez6'), n_code_len=6, n_input_len=6)

                                # 실시간 체결 데이터 init
                                row_pos = self.tbl_buy_list.rowCount()
                                self.tbl_buy_list.insertRow(row_pos)
                                cell = QTableWidgetItem(data.get('issue_namez40'))  # 종목명
                                self.tbl_buy_list.setItem(row_pos, 0, cell)
                                cell = QTableWidgetItem(data.get('issue_codez6'))  # 종목코드
                                self.tbl_buy_list.setItem(row_pos, 1, cell)
                                set_sys_log_messages('실시간 데이터 요청 : ' + data.get('issue_namez40') + '(' + data.get('issue_codez6') + ')')
            elif CA_RECEIVESISE in msg_data:  # 체결 종목 실시간 데이터 수신
                self.set_tbl_buy_list_row_data(msg_data.get(CA_RECEIVESISE).get('now_stock_data'))

        return super().nativeEvent(event_type, message)


class NamuhKeyring:
    SERVICE_NAME = 'NAMUH_BOT'
    CERT_PASS = '_CERT'

    def get_credential(self):
        try:
            credential_pw = keyring.get_credential(self.SERVICE_NAME, None)
            credential_cert_pw = keyring.get_credential(self.SERVICE_NAME + self.CERT_PASS, None)
            return credential_pw.username, credential_pw.password, credential_cert_pw.password
        except:
            return None, None, None

    def set_keyring(self, sz_id, sz_pw, sz_cert_pw):
        keyring.set_password(self.SERVICE_NAME, sz_id, sz_pw)
        keyring.set_password(self.SERVICE_NAME + self.CERT_PASS, sz_id, sz_cert_pw)


class LoginWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent

        uic.loadUi(resource_path('form.ui') + '/login.ui', self)
        self.show()

        sz_id, sz_pw, sz_cert_pw = NamuhKeyring().get_credential()
        self.ltxt_sz_id.setText(sz_id)
        self.ltxt_sz_pw.setText(sz_pw)
        self.ltxt_sz_cert_pw.setText(sz_cert_pw)

        if sz_id:  #
            self.chk_login.setChecked(True)

    def nativeEvent(self, event_type: typing.Union[QtCore.QByteArray, bytes, bytearray], message: sip.voidptr) -> typing.Tuple[bool, int]:
        msg_data = NamuhNativeEventFiler().wnd_proc_callback(event_type, message)
        if msg_data:
            if CA_CONNECTED in msg_data:  # 로그인 처리
                if self.chk_login.isChecked():
                    NamuhKeyring().set_keyring(sz_id=self.ltxt_sz_id.text(), sz_pw=self.ltxt_sz_pw.text(), sz_cert_pw=self.ltxt_sz_cert_pw.text())
                self.parent.run_dashboard()
            elif CA_RECEIVEMESSAGE in msg_data:  # 로그인 관련 메시지 수신(로그인 오류 등)
                if msg_data.get(CA_RECEIVEMESSAGE).get('msg_cd') != '00000':
                    QMessageBox.warning(self, '로그인', msg_data.get(CA_RECEIVEMESSAGE).get('user_msg'), QMessageBox.Ok)

        return super().nativeEvent(event_type, message)

    @pyqtSlot()
    def wmca_login(self):
        # print('wmca_login')
        if wmca.is_connected():
            QMessageBox.information(self, '로그인', '로그인 정보 있음', QMessageBox.Yes)
            self.parent.run_dashboard()
            return
        else:
            hwnd = int(self.winId())
            sz_id = self.ltxt_sz_id.text()
            sz_pw = self.ltxt_sz_pw.text()
            sz_cert_pw = self.ltxt_sz_cert_pw.text()
            is_hts = self.chk_is_hts.isChecked()
            self.set_is_hts(is_hts)

            if sz_id == '':
                QMessageBox.warning(self, '로그인', '아이디를 입력하세요.', QMessageBox.Ok)
                return
            elif sz_pw == '':
                QMessageBox.warning(self, '로그인', '비밀번호를 입력하세요.', QMessageBox.Ok)
                return
            elif sz_cert_pw == '':
                QMessageBox.warning(self, '로그인', '인증서 비밀번호를 입력하세요.', QMessageBox.Ok)
                return

        wmca.connect(hwnd=hwnd, sz_id=sz_id.encode(), sz_pw=sz_pw.encode(), sz_cert_pw=sz_cert_pw.encode())

    def set_is_hts(self, is_hts):
        self.is_hts = is_hts

        if is_hts:  # 모의투자일 경우
            wmca_server = "newmt.wontrading.com"
            wmca_port = "8400"
        else:  # 실투자일 경우
            wmca_server = "wmca.nhqv.com"
            wmca_port = "8200"

        import configparser

        config = configparser.ConfigParser()
        config.set("", "server", wmca_server)
        config.set("", "port", wmca_port)
        config.set("", "로그사용", "Y")
        python_exe_path = sys.base_exec_prefix  # os.path.dirname(sys.executable)
        # print("python_exe_path :", python_exe_path)
        with open(python_exe_path + r'\wmca.ini', 'w', encoding="utf-8") as configfile:  # python.exe 경로에 wmca.ini 생성(해당 경로에서 생성해야 적용됨)
            config.write(configfile, space_around_delimiters=False)  # space_around_delimiters : 환경변수의 공백여부 (공백이 있을 경우 읽지 못함)

        wmca.load()


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi(resource_path('form.ui') + '/mainWindow.ui', self)
        self.run_login()
        # self.run_dashboard()

    def set_init_UI(self, widget):
        self.setCentralWidget(widget)
        self.setGeometry(widget.geometry())
        self.center()
        self.show()

    def run_login(self):  # 로그인
        self.login = LoginWidget(self)
        self.set_init_UI(self.login)

    def run_dashboard(self):  # 대시보드
        self.dashboard = DashboardWidget(self)
        self.set_init_UI(self.dashboard)

    def center(self):  # 윈도우 창 화면 중앙 이동
        # geometry of the main window
        qr = self.frameGeometry()
        # center point of screen
        cp = QDesktopWidget().availableGeometry().center()
        # move rectangle's center point to screen's center point
        qr.moveCenter(cp)
        # top left of rectangle becomes top left of window centering it
        self.move(qr.topLeft())

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:  # 프로그램 종료
        wmca.detach_all()
        super().closeEvent(a0)


# exe 생성 명령어 : pyinstaller --add-data "bin;bin" --add-data "form.ui;form.ui" --onefile --noconsole namuh_bot.py
if __name__ == "__main__":
    wmca = WinDllWmca()  # 모바일증권 나무 DLL 로드

    app = QApplication(sys.argv)

    main = MainWindow()
    app.exec_()
